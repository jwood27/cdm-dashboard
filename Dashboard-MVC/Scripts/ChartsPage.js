﻿$(document).ready(function () {
    if (getParameterByName("clinic") === null) {
        $('#selectedClinic').prop("disabled", true);
        $('#PG').prop("checked", true);
    }
    else {
        $('#selectedGroup').prop("disabled", true);
        $('#Clinic').prop("checked", true);
    }

    $('input[type=radio][name=pgClinic]').on('change', function () {
        var radioValue = $("input[name='pgClinic']:checked").val();
        if (radioValue === 'PG') {
            $('#selectedClinic').prop("disabled", true);
            $('#selectedGroup').prop("disabled", false);
        }
        else {
            $('#selectedClinic').prop("disabled", false);
            $('#selectedGroup').prop("disabled", true);
        }
    });

    $('.datepicker').datepicker({
        format: "mm-yyyy",
        startView: "months",
        minViewMode: "months"
    });

    $('#updateButton').on("click", function () {
        var url = "Charts?year2=" + @ViewBag.y2 + "&month1=" + $('#m1').val() + "&month2=" + $('#m2').val();
        if ($("input[name='pgClinic']:checked").val() === 'PG')
        {
            url = url + "&group=" + $('#selectedGroup').val();
        }
        else {
            url = url + "&clinic=" + $('#selectedClinic').val();
        }
        this.href = url;
    });

    function getParameterByName(name, url) {
        if (!url) {
            url = window.location.href;
        }
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
});