﻿using Dashboard_MVC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;

namespace Dashboard_MVC.Helpers
{
    public class DataHelper
    {
        //get the data to populate the polar chart from NEWPATIENT stored procedure
        public static decimal GetDataNewPatient(DateTime startDate, DateTime endDate, int ty, string grouping)
        {
            var stuff = new Entities();

            ObjectParameter op = new ObjectParameter("PAT", typeof(decimal));
            stuff.NEW_PATIENT(op, startDate, endDate, (decimal)ty, grouping);

            if (op.Value.ToString() == "")
            {
                return 0;
            }
            else
            {
                return (decimal)op.Value;
            }
        }

        //get the data to populate the polar chart from NO_SHOW stored procedure
        public static decimal GetDataNoShow(DateTime startDate, DateTime endDate, int ty, string grouping)
        {
            var stuff = new Entities();

            ObjectParameter op = new ObjectParameter("PAT", typeof(decimal));
            stuff.NO_SHOW(op, startDate, endDate, (decimal)ty, grouping);

            if (op.Value.ToString() == "")
            {
                return 0;
            }
            else
            {
                return (decimal)op.Value;
            }
        }

        //get the data to populate the polar chart from VISITS_ACTUAL stored procedure
        public static decimal GetDataVisitsActual(DateTime startDate, DateTime endDate, int ty, string grouping)
        {
            var stuff = new Entities();

            ObjectParameter op = new ObjectParameter("VISIT", typeof(decimal));
            stuff.VISITS_ACTUAL(op, startDate, endDate, (decimal)ty, grouping);

            if (op.Value.ToString() == "")
            {
                return 0;
            }
            else
            {
                return (decimal)op.Value;
            }
        }

        //get the data to populate the polar chart from VISITS_SCHEDULED stored procedure
        public static decimal GetDataVisitsScheduled(DateTime startDate, DateTime endDate, int ty, string grouping)
        {
            var stuff = new Entities();

            ObjectParameter op = new ObjectParameter("VISIT", typeof(decimal));
            stuff.VISITS_SCHEDULED(op, startDate, endDate, (decimal)ty, grouping);

            if (op.Value.ToString() == "")
            {
                return 0;
            }
            else
            {
                return (decimal)op.Value;
            }
        }

        //get the data to populate the polar chart from GROSS_REV stored procedure
        public static decimal GetDataGrossRev(DateTime startDate, DateTime endDate, int ty, string grouping)
        {
            var stuff = new Entities();

            ObjectParameter op = new ObjectParameter("REV", typeof(decimal));
            stuff.GROSS_REV(op, startDate, endDate, (decimal)ty, grouping);

            if (op.Value.ToString() == "")
            {
                return 0;
            }
            else
            {
                return (decimal)op.Value;
            }
        }

        //get the data to populate the polar chart from CHAIR_SCHE stored procedure
        public static decimal GetDataChairSche(DateTime startDate, DateTime endDate, int ty, string grouping)
        {
            var stuff = new Entities();

            ObjectParameter op = new ObjectParameter("Sche", typeof(decimal));
            stuff.CHAIR_SCHE(op, startDate, endDate, (decimal)ty, grouping);

            if (op.Value.ToString() == "")
            {
                return 0;
            }
            else
            {
                return (decimal)op.Value;
            }
        }

        //get the data to populate the polar chart from TXPLAN_APPR stored procedure
        public static decimal GetDataTXPlanAppr(DateTime startDate, DateTime endDate, int ty, string grouping)
        {
            var stuff = new Entities();

            ObjectParameter op = new ObjectParameter("Appr", typeof(decimal));
            stuff.TXPLAN_APPR(op, startDate, endDate, (decimal)ty, grouping);

            if (op.Value.ToString() == "")
            {
                return 0;
            }
            else
            {
                return (decimal)op.Value;
            }
        }

        //get the data to populate the polar chart from TXPLAN_APPR stored procedure
        public static decimal GetDataTXPlanAccpt(DateTime startDate, DateTime endDate, int ty, string grouping)
        {
            var stuff = new Entities();

            ObjectParameter op = new ObjectParameter("Appr", typeof(decimal));
            stuff.TXPLAN_ACCPT(op, startDate, endDate, (decimal)ty, grouping);

            if (op.Value.ToString() == "")
            {
                return 0;
            }
            else
            {
                return (decimal)op.Value;
            }
        }

        //get the data to populate the polar chart from TXPLAN_APPR stored procedure
        public static decimal GetDataMckAdjust(DateTime startDate, DateTime endDate, int ty, string grouping)
        {
            var stuff = new Entities();

            ObjectParameter op = new ObjectParameter("Amt", typeof(decimal));
            stuff.MCK_ADJUST(op, startDate, endDate, (decimal)ty, grouping);

            if (op.Value.ToString() == "")
            {
                return 0;
            }
            else
            {
                return (decimal)op.Value;
            }
        }

        //get the data to populate the polar chart from TXPLAN_APPR stored procedure
        //not finished yet
        public static decimal GetDataMckDeny(DateTime startDate, DateTime endDate, int ty, string grouping)
        {
            var stuff = new Entities();

            ObjectParameter op = new ObjectParameter("Appr", typeof(decimal));
            stuff.TXPLAN_APPR(op, startDate, endDate, (decimal)ty, grouping);

            if (op.Value.ToString() == "")
            {
                return 0;
            }
            else
            {
                return (decimal)op.Value;
            }
        }

        //get the data to populate the polar chart from TXPLAN_APPR stored procedure
        public static decimal GetDataSafteyDrys(DateTime startDate, DateTime endDate, int ty, string grouping)
        {
            var stuff = new Entities();

            ObjectParameter op = new ObjectParameter("Pat", typeof(decimal));
            stuff.SAFETY_DRYS(op, startDate, endDate, (decimal)ty, grouping);

            if (op.Value.ToString() == "")
            {
                return 0;
            }
            else
            {
                return (decimal)op.Value;
            }
        }

        //get the data to populate the polar chart from TXPLAN_APPR stored procedure
        public static decimal GetDataSafteyImp(DateTime startDate, DateTime endDate, int ty, string grouping)
        {
            var stuff = new Entities();

            ObjectParameter op = new ObjectParameter("Pat", typeof(decimal));
            stuff.SAFETY_IMP(op, startDate, endDate, (decimal)ty, grouping);

            if (op.Value.ToString() == "")
            {
                return 0;
            }
            else
            {
                return (decimal)op.Value;
            }
        }

        //get the data to populate the polar chart from TXPLAN_APPR stored procedure
        public static decimal GetDataSafteyCBCT(DateTime startDate, DateTime endDate, int ty, string grouping)
        {
            var stuff = new Entities();

            ObjectParameter op = new ObjectParameter("Pat", typeof(decimal));
            stuff.SAFETY_CBCT(op, startDate, endDate, (decimal)ty, grouping);

            if (op.Value.ToString() == "")
            {
                return 0;
            }
            else
            {
                return (decimal)op.Value;
            }
        }

        //get the data to populate the polar chart from TXPLAN_APPR stored procedure
        //public static decimal GetDataClinicList(string name)
        //{
        //    var stuff = new Entities();

        //    ObjectParameter op = new ObjectParameter("clinic", typeof(decimal));
        //    stuff.GETCLINIC(op, startDate, endDate, (decimal)ty, grouping);

        //    if (op.Value.ToString() == "")
        //    {
        //        return 0;
        //    }
        //    else
        //    {
        //        return (decimal)op.Value;
        //    }
        //}
    }
}