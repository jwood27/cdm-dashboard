﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChartDirector;

namespace Dashboard_MVC.Helpers
{
    public class ChartDirectorHelper
    {
        //create a polar chart for patient metrics
        public static void createChartPolarPatient(RazorChartViewer viewer, double[] data, string[] labels, string grouping, string month1Lbl, string month2Lbl)
        {
            // The data for the chart
            //double[] data = { 90, 60, 65, 75, 40 };
            //double[] data = { 131.25, 143.478260869565, 100, 54.5454545454545 };

            // The labels for the chart
            //string[] labels = { "Speed", "Reliability", "Comfort", "Safety", "Efficiency" };
            //string[] labels = { "Pt visits scheduled", "Pt visits actual", "New pt", "New Tx plans approved", "No show", "Chair utilization scheduled", "Chair utilization actual", "Gross rev(RVU)" };

            // Create a PolarChart object of size 450 x 350 pixels
            PolarChart c = new PolarChart(400, 350);

            //from ZZ
            ChartDirector.TextBox t = c.addTitle("<*underline=1*>" + grouping + " Level: Operational Metrics");

            // Set center of plot area at (225, 185) with radius 150 pixels
            c.setPlotArea(225, 185, 100);

            // Add an area layer to the polar chart
            c.addAreaLayer(data, 0x9999ff);

            // Set the labels to the angular axis as spokes
            c.angularAxis().setLabels(labels);

            c.radialAxis().setLinearScale(0, 100, 0);

            // Output the chart
            viewer.Image = c.makeWebImage(Chart.PNG);

            // Include tool tip for the chart
            viewer.ImageMap = c.getHTMLImageMap("", "", "title='{label}: score = {value}'");
        }

        //create a polar chart for finance metrics
        public static void createChartPolarFinance(RazorChartViewer viewer, double[] data, string[] labels, string grouping, string month1Lbl, string month2Lbl)
        {
            // The data for the chart
            //double[] data = { 90, 60, 65, 75, 40 };
            //double[] data = { 111.25, 135.478260869565, 80 };

            // The labels for the chart
            //string[] labels = { "Speed", "Reliability", "Comfort", "Safety", "Efficiency" };
            //string[] labels = { "Pt visits scheduled", "Pt visits actual", "New pt", "New Tx plans approved", "No show", "Chair utilization scheduled", "Chair utilization actual", "Gross rev(RVU)" };

            // Create a PolarChart object of size 450 x 350 pixels
            PolarChart c = new PolarChart(400, 350);

            //from ZZ
            ChartDirector.TextBox t = c.addTitle("<*underline=1*>" + grouping + " Level: Finance Metrics");

            // Set center of plot area at (225, 185) with radius 150 pixels
            c.setPlotArea(225, 185, 100);

            // Add an area layer to the polar chart
            c.addAreaLayer(data, 0x9999ff);

            // Set the labels to the angular axis as spokes
            c.angularAxis().setLabels(labels);

            c.radialAxis().setLinearScale(0, 100, 0);

            // Output the chart
            viewer.Image = c.makeWebImage(Chart.PNG);

            // Include tool tip for the chart
            viewer.ImageMap = c.getHTMLImageMap("", "", "title='{label}: score = {value}'");
        }

        //create a polar chart for student metrics
        public static void createChartPolarStudent(RazorChartViewer viewer, double[] data1, string[] labels, string grouping, string month1Lbl, string month2Lbl)
        {
            // The data for the chart
            double[] data = { 71.25, 115.478260869565, 120, 82, 98 };

            // The labels for the chart
            //string[] labels = { "Speed", "Reliability", "Comfort", "Safety", "Efficiency" };
            //string[] labels = { "Pt visits scheduled", "Pt visits actual", "New pt", "New Tx plans approved", "No show", "Chair utilization scheduled", "Chair utilization actual", "Gross rev(RVU)" };

            // Create a PolarChart object of size 450 x 350 pixels
            PolarChart c = new PolarChart(400, 350);

            //from ZZ
            ChartDirector.TextBox t = c.addTitle("<*underline=1*>" + grouping + " Level: Student Metrics");

            // Set center of plot area at (225, 185) with radius 150 pixels
            c.setPlotArea(225, 185, 100);

            // Add an area layer to the polar chart
            c.addAreaLayer(data, 0x9999ff);

            // Set the labels to the angular axis as spokes
            c.angularAxis().setLabels(labels);

            c.radialAxis().setLinearScale(0, 100, 0);

            // Output the chart
            viewer.Image = c.makeWebImage(Chart.PNG);

            // Include tool tip for the chart
            viewer.ImageMap = c.getHTMLImageMap("", "", "title='{label}: score = {value}'");
        }

        //create a polar chart for saftey metrics
        public static void createChartPolarSaftey(RazorChartViewer viewer, double[] data, string[] labels, string grouping, string month1Lbl, string month2Lbl)
        {
            // The data for the chart
            //double[] data = { 90, 60, 65, 75, 40 };
            //double[] data = { 123, 76, 80 };

            // The labels for the chart
            //string[] labels = { "Pt visits scheduled", "Pt visits actual", "New pt", "New Tx plans approved", "No show", "Chair utilization scheduled", "Chair utilization actual", "Gross rev(RVU)" };

            // Create a PolarChart object of size 450 x 350 pixels
            PolarChart c = new PolarChart(400, 350);

            //from ZZ
            ChartDirector.TextBox t = c.addTitle("<*underline=1*>" + grouping + " Level: Saftey Metrics");

            // Set center of plot area at (225, 185) with radius 150 pixels
            c.setPlotArea(225, 185, 100);

            // Add an area layer to the polar chart
            c.addAreaLayer(data, 0x9999ff);

            // Set the labels to the angular axis as spokes
            c.angularAxis().setLabels(labels);

            c.radialAxis().setLinearScale(0, 100, 0);

            // Output the chart
            viewer.Image = c.makeWebImage(Chart.PNG);

            // Include tool tip for the chart
            viewer.ImageMap = c.getHTMLImageMap("", "", "title='{label}: score = {value}'");
        }

        //create a multi-bar chart for patient metrics
        public static void createChartMultiPatient(RazorChartViewer viewer, double[] data1, double[] data2, string[] labels, string grouping, string year1Lbl, string year2Lbl)
        {
            // The data for the bar chart
            //double[] data1 = { 90, 60, 65, 75 };
            //double[] data2 = { 93, 50, 68, 84 };

            // The labels for the bar chart
            //string[] labels = { "Pt visits scheduled", "Pt visits actual", "New pt", "New Tx plans approved", "No show", "Chair utilization scheduled", "Chair utilization actual", "Gross rev(RVU)" };

            // Create a XYChart object of size 250 x 250 pixels
            XYChart c = new XYChart(400, 350);

            // Add a title to the chart using 10 pt Arial font - from ZZ
            ChartDirector.TextBox t = c.addTitle("<*underline=1*>" + grouping + " Level: Operational Metrics " + year1Lbl + " vs " + year2Lbl);

            // Set the plotarea at (30, 20) and of size 200 x 200 pixels
            c.setPlotArea(50, 0, 350, 250, 0xFFFFFF, 0xFFFFFF, Chart.Transparent);

            //from ZZ
            c.addLegend(55, 18, false, "", 8).setBackground(Chart.Transparent);
            c.yAxis().setTopMargin(20);
            c.xAxis().setLabels(labels).setFontAngle(20);

            // Add a bar chart layer using the given data
            //c.addBarLayer(data1);

            //from ZZ
            BarLayer layer = c.addBarLayer2(Chart.Side, 3);
            layer.addDataSet(data2, 0xCC9933, year1Lbl);
            layer.addDataSet(data1, 0x336699, year2Lbl);

            // Set the labels on the x axis.
            c.xAxis().setLabels(labels);

            // Output the chart
            viewer.Image = c.makeWebImage(Chart.PNG);

            // Include tool tip for the chart
            viewer.ImageMap = c.getHTMLImageMap("", "", "title='{xLabel}: {value}'");
        }

        //create a multi-bar chart for finance metrics
        public static void createChartMultiFinance(RazorChartViewer viewer, double[] data1, double[] data2, string[] labels, string grouping, string year1Lbl, string year2Lbl)
        {
            // The data for the bar chart
            //double[] data1 = { 90, 60, 65 };
            //double[] data2 = { 93, 50, 68 };

            // The labels for the bar chart
            //string[] labels = { "Pt visits scheduled", "Pt visits actual", "New pt", "New Tx plans approved", "No show", "Chair utilization scheduled", "Chair utilization actual", "Gross rev(RVU)" };

            // Create a XYChart object of size 250 x 250 pixels
            XYChart c = new XYChart(400, 350);

            // Add a title to the chart using 10 pt Arial font - from ZZ
            ChartDirector.TextBox t = c.addTitle("<*underline=1*>" + grouping + " Level: Finance Metrics " + year1Lbl + " vs " + year2Lbl);

            // Set the plotarea at (30, 20) and of size 200 x 200 pixels
            c.setPlotArea(50, 0, 350, 250, 0xFFFFFF, 0xFFFFFF, Chart.Transparent);

            //from ZZ
            c.addLegend(55, 18, false, "", 8).setBackground(Chart.Transparent);
            c.yAxis().setTopMargin(20);
            c.xAxis().setLabels(labels).setFontAngle(20);

            // Add a bar chart layer using the given data
            //c.addBarLayer(data1);

            //from ZZ
            BarLayer layer = c.addBarLayer2(Chart.Side, 3);
            layer.addDataSet(data1, 0xCC9933, year1Lbl);
            layer.addDataSet(data2, 0x336699, year2Lbl);

            // Set the labels on the x axis.
            c.xAxis().setLabels(labels);

            // Output the chart
            viewer.Image = c.makeWebImage(Chart.PNG);

            // Include tool tip for the chart
            viewer.ImageMap = c.getHTMLImageMap("", "", "title='{xLabel}: {value}'");
        }

        //create a multi-bar chart for student metrics
        public static void createChartMultiStudent(RazorChartViewer viewer, double[] data3, double[] data4, string[] labels, string grouping, string year1Lbl, string year2Lbl)
        {
            // The data for the bar chart
            double[] data1 = { 90, 60, 65, 75, 80 };
            double[] data2 = { 93, 50, 68, 84, 62 };

            // The labels for the bar chart
            //string[] labels = { "Pt visits scheduled", "Pt visits actual", "New pt", "New Tx plans approved", "No show", "Chair utilization scheduled", "Chair utilization actual", "Gross rev(RVU)" };

            // Create a XYChart object of size 250 x 250 pixels
            XYChart c = new XYChart(400, 350);

            // Add a title to the chart using 10 pt Arial font - from ZZ
            ChartDirector.TextBox t = c.addTitle("<*underline=1*>" + grouping + " Level: Student Metrics " + year1Lbl + " vs " + year2Lbl);

            // Set the plotarea at (30, 20) and of size 200 x 200 pixels
            c.setPlotArea(50, 0, 350, 250, 0xFFFFFF, 0xFFFFFF, Chart.Transparent);

            //from ZZ
            c.addLegend(55, 18, false, "", 8).setBackground(Chart.Transparent);
            c.yAxis().setTopMargin(20);
            c.xAxis().setLabels(labels).setFontAngle(20);

            // Add a bar chart layer using the given data
            //c.addBarLayer(data1);

            //from ZZ
            BarLayer layer = c.addBarLayer2(Chart.Side, 3);
            layer.addDataSet(data1, 0xCC9933, year1Lbl);
            layer.addDataSet(data2, 0x336699, year2Lbl);

            // Set the labels on the x axis.
            c.xAxis().setLabels(labels);

            // Output the chart
            viewer.Image = c.makeWebImage(Chart.PNG);

            // Include tool tip for the chart
            viewer.ImageMap = c.getHTMLImageMap("", "", "title='{xLabel}: {value}'");
        }

        //create a multi-bar chart for saftey metrics
        public static void createChartMultiSaftey(RazorChartViewer viewer, double[] data1, double[] data2, string[] labels, string grouping, string year1Lbl, string year2Lbl)
        {
            // The data for the bar chart
            //double[] data1 = { 90, 60, 65 };
            //double[] data2 = { 93, 50, 68 };

            // The labels for the bar chart
            //string[] labels = { "Pt visits scheduled", "Pt visits actual", "New pt", "New Tx plans approved", "No show", "Chair utilization scheduled", "Chair utilization actual", "Gross rev(RVU)" };

            // Create a XYChart object of size 250 x 250 pixels
            XYChart c = new XYChart(400, 350);

            // Add a title to the chart using 10 pt Arial font - from ZZ
            ChartDirector.TextBox t = c.addTitle("<*underline=1*>" + grouping + " Level: Saftey Metrics " + year1Lbl + " vs " + year2Lbl);
            // Set the plotarea at (30, 20) and of size 200 x 200 pixels
            c.setPlotArea(50, 0, 350, 250, 0xFFFFFF, 0xFFFFFF, Chart.Transparent);

            //from ZZ
            c.addLegend(55, 18, false, "", 8).setBackground(Chart.Transparent);
            c.yAxis().setTopMargin(20);
            c.xAxis().setLabels(labels).setFontAngle(20);

            // Add a bar chart layer using the given data
            //c.addBarLayer(data1);

            //from ZZ
            BarLayer layer = c.addBarLayer2(Chart.Side, 3);
            layer.addDataSet(data1, 0xCC9933, year1Lbl);
            layer.addDataSet(data2, 0x336699, year2Lbl);

            // Set the labels on the x axis.
            c.xAxis().setLabels(labels);

            // Output the chart
            viewer.Image = c.makeWebImage(Chart.PNG);

            // Include tool tip for the chart
            viewer.ImageMap = c.getHTMLImageMap("", "", "title='{xLabel}: {value}'");
        }
    }
}