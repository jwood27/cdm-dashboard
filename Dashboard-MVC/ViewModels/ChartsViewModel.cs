﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dashboard_MVC.Models;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Dashboard_MVC.ViewModels
{
    public class ChartsViewModel 
    {
        public string selectedGroup { get; set; }
        public string selectedClinic { get; set; }
        public int selectedType { get; set; }
        public DateTime? month1 { get; set; }
        public DateTime? month2 { get; set; }
        public List<string> ClinicList { get; set; }
        public List<string> PGroupList { get; set; }
    }
}