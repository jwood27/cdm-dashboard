﻿using System;
using System.Linq;
using System.Web.Mvc;
using ChartDirector;
using Dashboard_MVC.Models;
using System.Data.Entity.Core.Objects;
using System.Collections.Generic;
using Dashboard_MVC.Helpers;

namespace Dashboard_MVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        //[Route("{home}/{charts}/{group?}")]
        public ActionResult Charts(string group, string clinic, int? year2, DateTime? month1, DateTime? month2)
        {
            ViewBag.Message = "Your chart page.";
            ViewBag.Viewer = new RazorChartViewer[4];

            //number of metrics to display
            int metricnum = 6;

            double[] curMetricNum = new double[metricnum];
            double[] prevMetricNum = new double[metricnum];
            double[] percMetricNum = new double[metricnum];            

            double[] curMetricNumB = new double[metricnum];
            double[] prevMetricNumB = new double[metricnum];                             

            var chart = new Dashboard_MVC.ViewModels.ChartsViewModel();

            string[] clinics = { "CDM", "143", "164", "52", "AEGD", "BREAD", "EMERG", "ENDO", "GPR", "ICAT", "IF", "IMPLAN", "NOC", "OFP", "OMFS", "OMFSFP", "ORTHO", "ORTHOS", "PEDO", "PERIO", "PROM", "PROSTH", "PS180", "TMA", "TRIAG", "VAN", "VC7", "VC8", "VC9HYG" };
            string[] pgroups = { "CDM", "AEGD", "ASSIST", "DCSCHL", "DPH", "DS12", "DS13", "DS14", "DS15", "DS16", "DS17", "DS18", "ENDO", "FAC", "FAC-DC", "FAC-PD", "FAC-TR", "GPR", "HYG", "HYG-DC", "HYG-PD", "IMP", "IMPINT", "OMFS", "OPATH", "ORTHO", "PEDI", "PERIO", "PROSTH", "UG3", "UG4" };

            chart.ClinicList = new List<string>(clinics);
            chart.PGroupList = new List<string>(pgroups);

            //set defaults
            chart.selectedGroup = (group == null) ? "CDM" : group;
            chart.selectedClinic = (clinic == null) ? "CDM" : clinic;
            
            int? y2 = (year2 == null) ? 0 : year2;

            //set default dates depending on chart type
            if (y2 == 0)
            {
                chart.month1 = (month1 == null) ? new DateTime(2016, 1, 1) : month1;
                chart.month2 = (month2 == null) ? new DateTime(2017, 1, 1) : month2;
            }
            else
            {
                chart.month1 = (month1 == null) ? new DateTime(2016, 1, 1) : month1;
                chart.month2 = (month2 == null) ? new DateTime(2017, 1, 1) : month2;
            }

            ViewBag.y2 = y2;
            int typ;

            //figure out if search is by clinic (1) or provider group (2)
            if (group == null && clinic != null)
            {
                typ = 1;
            }
            else if(group != null && clinic == null && group != "CDM" )
            {
                typ = 2;
            }
            else
            {
                typ = 1;
            }
            
            string[] metricLabel1 = { "New pt", "No show", "Visits actual", "Visits scheduled", "TX Plan Appr", "TX Plan Accept" };
            string[] metricLabel2 = { "RVU", "Adjustments", "Denials" };
            string[] metricLabel3 = { "Endo", "Perio", "Removable", "C and B", "Operative" };
            string[] metricLabel4 = { "Implant removal", "Dry sockets", "CBC retakes" };

            double[] multiFinanceMetric1 = { 16100123, 13600123, 8300123 };
            double[] multiFinanceMetric2 = { 15400123, 14300123, 9400123 };
            double[] multiFinanceMetric3 = new double[metricLabel2.Count()];

            double[] multiSafteyMetric1 = { 104, 88, 120 };
            double[] multiSafteyMetric2 = { 111, 86, 92 };
            double[] multiSafteyMetric3 = new double[metricLabel4.Count()];

            string selG = typ == 1 ? chart.selectedClinic : chart.selectedGroup;

            if (y2 == 0)
            {
                //calcuate startdate and end date from MM-yyyy date
                DateTime startM1 = new DateTime(chart.month1.Value.Year, chart.month1.Value.Month, 1);
                DateTime endM1 = new DateTime(chart.month1.Value.Year, chart.month1.Value.Month, DateTime.DaysInMonth(chart.month1.Value.Year, chart.month1.Value.Month));
                DateTime startM2 = new DateTime(chart.month2.Value.Year, chart.month2.Value.Month, 1);
                DateTime endM2 = new DateTime(chart.month2.Value.Year, chart.month2.Value.Month, DateTime.DaysInMonth(chart.month2.Value.Year, chart.month2.Value.Month));

                //populate array for current month for polar
                curMetricNum[0] = (double)DataHelper.GetDataNewPatient(startM1, endM1, typ, selG);
                curMetricNum[1] = (double)DataHelper.GetDataNoShow(startM1, endM1, typ, selG);
                curMetricNum[2] = (double)DataHelper.GetDataVisitsActual(startM1, endM1, typ, selG);
                curMetricNum[3] = (double)DataHelper.GetDataVisitsScheduled(startM1, endM1, typ, selG);
                curMetricNum[4] = (double)DataHelper.GetDataTXPlanAppr(startM1, endM1, typ, selG);
                curMetricNum[5] = (double)DataHelper.GetDataTXPlanAccpt(startM1, endM1, typ, selG);

                //populate array for previous month for polar
                prevMetricNum[0] = (double)DataHelper.GetDataNewPatient(startM2, endM2, typ, selG);
                prevMetricNum[1] = (double)DataHelper.GetDataNoShow(startM2, endM2, typ, selG);
                prevMetricNum[2] = (double)DataHelper.GetDataVisitsActual(startM2, endM2, typ, selG);
                prevMetricNum[3] = (double)DataHelper.GetDataVisitsScheduled(startM2, endM2, typ, selG);
                prevMetricNum[4] = (double)DataHelper.GetDataTXPlanAppr(startM2, endM2, typ, selG);
                prevMetricNum[5] = (double)DataHelper.GetDataTXPlanAccpt(startM2, endM2, typ, selG);

                //set metrics in finance bar chart (will add deny when it is finished)
                multiFinanceMetric1[0] = (double)DataHelper.GetDataGrossRev(startM1, endM1, typ, selG);
                multiFinanceMetric2[0] = (double)DataHelper.GetDataGrossRev(startM2, endM2, typ, selG);
                multiFinanceMetric1[1] = (double)DataHelper.GetDataMckAdjust(startM1, endM1, typ, selG);
                multiFinanceMetric2[1] = (double)DataHelper.GetDataMckAdjust(startM2, endM2, typ, selG);
                //multiFinanceMetric1[2] = (double)DataHelper.GetDataMckDeny(startM1, endM1, typ, selG);
                //multiFinanceMetric2[2] = (double)DataHelper.GetDataMckDeny(startM2, endM2, typ, selG);

                //set metrics in saftey bar chart (will add cbct when it is finished)
                multiSafteyMetric1[0] = (double)DataHelper.GetDataSafteyImp(startM1, endM1, typ, selG);
                multiSafteyMetric2[0] = (double)DataHelper.GetDataSafteyImp(startM2, endM2, typ, selG);
                multiSafteyMetric1[1] = (double)DataHelper.GetDataSafteyDrys(startM1, endM1, typ, selG);
                multiSafteyMetric2[1] = (double)DataHelper.GetDataSafteyDrys(startM2, endM2, typ, selG);
                multiSafteyMetric1[2] = (double)DataHelper.GetDataSafteyCBCT(startM1, endM1, typ, selG);
                multiSafteyMetric2[2] = (double)DataHelper.GetDataSafteyCBCT(startM2, endM2, typ, selG);

                //calculate percentage for finance metrics
                for (int i = 0; i < multiFinanceMetric1.Count(); i++)
                {
                    if (multiFinanceMetric2[i] != 0)
                    {
                        multiFinanceMetric3[i] = (multiFinanceMetric2[i] / multiFinanceMetric1[i]) * 100;
                    }
                    else
                    {
                        multiFinanceMetric3[i] = 0;
                    }
                }

                //calculate percentage for saftey metrics
                for (int i = 0; i < multiSafteyMetric1.Count(); i++)
                {
                    if (multiSafteyMetric2[i] != 0)
                    {
                        multiSafteyMetric3[i] = (multiSafteyMetric2[i] / multiSafteyMetric1[i]) * 100;
                    }
                    else
                    {
                        multiSafteyMetric3[i] = 0;
                    }
                }

                //calculate percentage for polar chart
                for (int i = 0; i < curMetricNum.Count(); i++)
                {
                    if (prevMetricNum[i] != 0)
                    {
                        percMetricNum[i] = (prevMetricNum[i] / curMetricNum[i]) * 100;
                    }
                    else
                    {
                        percMetricNum[i] = 0;
                    }
                }

                ChartDirectorHelper.createChartPolarPatient(ViewBag.Viewer[0] = new RazorChartViewer(HttpContext, "chart1"), percMetricNum, metricLabel1, selG, startM1.Month.ToString(), startM2.Month.ToString());
                ChartDirectorHelper.createChartPolarFinance(ViewBag.Viewer[1] = new RazorChartViewer(HttpContext, "chart2"), multiFinanceMetric3, metricLabel2, selG, startM1.Month.ToString(), startM2.Month.ToString());
                ChartDirectorHelper.createChartPolarStudent(ViewBag.Viewer[2] = new RazorChartViewer(HttpContext, "chart3"), percMetricNum, metricLabel3, selG, startM1.Month.ToString(), startM2.Month.ToString());
                ChartDirectorHelper.createChartPolarSaftey(ViewBag.Viewer[3] = new RazorChartViewer(HttpContext, "chart4"), multiSafteyMetric3, metricLabel4, selG, startM1.Month.ToString(), startM2.Month.ToString());
            }
            else
            {
                //calcuate startdate and end date from MM-yyyy date
                DateTime startY1 = new DateTime(chart.month1.Value.Year, 1, 1);
                DateTime endY1 = new DateTime(chart.month1.Value.Year, 12, DateTime.DaysInMonth(chart.month1.Value.Year, chart.month1.Value.Month));
                DateTime startY2 = new DateTime(chart.month2.Value.Year, 1, 1);
                DateTime endY2 = new DateTime(chart.month2.Value.Year, 12, DateTime.DaysInMonth(chart.month2.Value.Year, chart.month2.Value.Month));

                //populate array for current month for multibar
                curMetricNumB[0] = (double)DataHelper.GetDataNewPatient(startY1, endY1, typ, selG);
                curMetricNumB[1] = (double)DataHelper.GetDataNoShow(startY1, endY1, typ, selG);
                curMetricNumB[2] = (double)DataHelper.GetDataVisitsActual(startY1, endY1, typ, selG);
                curMetricNumB[3] = (double)DataHelper.GetDataVisitsScheduled(startY1, endY1, typ, selG);
                curMetricNumB[4] = (double)DataHelper.GetDataTXPlanAppr(startY1, endY1, typ, selG);
                curMetricNumB[5] = (double)DataHelper.GetDataTXPlanAccpt(startY1, endY1, typ, selG);

                //populate array for previous month for multibar
                prevMetricNumB[0] = (double)DataHelper.GetDataNewPatient(startY2, endY2, typ, selG);
                prevMetricNumB[1] = (double)DataHelper.GetDataNoShow(startY2, endY2, typ, selG);
                prevMetricNumB[2] = (double)DataHelper.GetDataVisitsActual(startY2, endY2, typ, selG);
                prevMetricNumB[3] = (double)DataHelper.GetDataVisitsScheduled(startY2, endY2, typ, selG);
                prevMetricNumB[4] = (double)DataHelper.GetDataTXPlanAppr(startY2, endY2, typ, selG);
                prevMetricNumB[5] = (double)DataHelper.GetDataTXPlanAccpt(startY2, endY2, typ, selG);

                //set RVU in finance bar chart (will expand with more stored procs)
                multiFinanceMetric1[0] = (double)DataHelper.GetDataGrossRev(startY1, endY1, typ, selG);
                multiFinanceMetric2[0] = (double)DataHelper.GetDataGrossRev(startY2, endY2, typ, selG);
                multiFinanceMetric1[1] = (double)DataHelper.GetDataMckAdjust(startY1, endY1, typ, selG);
                multiFinanceMetric2[1] = (double)DataHelper.GetDataMckAdjust(startY2, endY2, typ, selG);

                //set RVU in saftey bar chart (will expand with more stored procs)
                multiSafteyMetric1[0] = (double)DataHelper.GetDataSafteyImp(startY1, endY1, typ, selG);
                multiSafteyMetric2[0] = (double)DataHelper.GetDataSafteyImp(startY2, endY2, typ, selG);
                multiSafteyMetric1[1] = (double)DataHelper.GetDataSafteyDrys(startY1, endY1, typ, selG);
                multiSafteyMetric2[1] = (double)DataHelper.GetDataSafteyDrys(startY2, endY2, typ, selG);
                multiSafteyMetric1[2] = (double)DataHelper.GetDataSafteyCBCT(startY1, endY1, typ, selG);
                multiSafteyMetric2[2] = (double)DataHelper.GetDataSafteyCBCT(startY2, endY2, typ, selG);

                //create multi bar charts
                ChartDirectorHelper.createChartMultiPatient(ViewBag.Viewer[0] = new RazorChartViewer(HttpContext, "chart1"), prevMetricNumB, curMetricNumB, metricLabel1, selG, startY1.Year.ToString(), startY2.Year.ToString());
                ChartDirectorHelper.createChartMultiFinance(ViewBag.Viewer[1] = new RazorChartViewer(HttpContext, "chart2"), multiFinanceMetric1, multiFinanceMetric2, metricLabel2, selG, startY1.Year.ToString(), startY2.Year.ToString());
                ChartDirectorHelper.createChartMultiStudent(ViewBag.Viewer[2] = new RazorChartViewer(HttpContext, "chart3"), prevMetricNumB, curMetricNumB, metricLabel3, selG, startY1.Year.ToString(), startY2.Year.ToString());
                ChartDirectorHelper.createChartMultiSaftey(ViewBag.Viewer[3] = new RazorChartViewer(HttpContext, "chart4"), multiSafteyMetric1, multiSafteyMetric2, metricLabel4, selG, startY1.Year.ToString(), startY2.Year.ToString());
            }
            return View(chart);
        }
    }
}